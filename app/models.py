from app import db


class DicitionaryItem(db.Model):
    __tablename__ = 'dict'

    __table_args__ = (
        db.PrimaryKeyConstraint('group', 'ngram'),
    )

    uuid = db.Column(db.String(40), primary_key=True)
    group = db.Column(db.String(40), nullable=False)
    ngram = db.Column(db.String(255), nullable=False, unique=True)
