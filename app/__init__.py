import os
import logging


from logging.handlers import RotatingFileHandler


from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app.conf.settings import Settings
from app.entrypoints import do_seed


app = Flask(__name__)
app.config.from_object(Settings)


app.cli.add_command(do_seed)


db = SQLAlchemy(app)
db.init_app(app)


from app import models, views

app.add_url_rule('/', 'videos', view_func=views.ngrams)


if not os.path.exists(Settings.LOGS_PATH):
    os.mkdir(Settings.LOGS_PATH)
file_handler = RotatingFileHandler(os.path.join(Settings.LOGS_PATH, 'errors.log'), maxBytes=10240, backupCount=10)
file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
file_handler.setLevel(logging.INFO)
app.logger.addHandler(file_handler)

app.logger.setLevel(logging.WARNING)
app.logger.info('App starting: {}'.format(Settings.BASE_URL))
