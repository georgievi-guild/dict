var channels_search = {
  query: undefined,
  order: undefined,
  page_token: undefined,
  loading: false,
  templates: undefined,
  init: function(query, order){
    channels_search.query = query || "";
    channels_search.order = order || "videoCount";

    $("#channels").append('<div class="row"></div>');

    channels_search.templates = {
      channel: Handlebars.compile($("#channel-template").html()),
      search_home: Handlebars.compile($("#search-home").html()),
      search_no_results: Handlebars.compile($("#search-no-results").html()),
      search_spinner: Handlebars.compile($("#search-spinner").html()),
      search_more: Handlebars.compile($("#search-more").html()),
    };
    channels_search.search();
  },
  search: function(){
    if (channels_search.loading){ return; }
    if (channels_search.page_token === null){ return; }
    if (!channels_search.query){
      channels_search.reset();
      return;
    }
    channels_search.loading = true;

    $("#channels .search-well").remove();
    $("#channels").append(channels_search.templates.search_spinner());
    $("#inputSearchChannel").val(channels_search.query);

    var req = {
      q: channels_search.query,
      order: channels_search.order,
      page_token: channels_search.page_token
    }

    $.getJSON(urls.api.channel_search, req, function(resp){
      if (!channels_search.loading){ return; }

      $("#channels .search-well").remove();

      if(resp.channels.length === 0){
        if (channels_search.page_token === null){

        } else {
          $("#channels").append(channels_search.templates.search_no_results({ entities: "channels" }))
        }
      } else {
        for(var i in resp.channels){
          var channel = resp.channels[i];
          var channel_data = {
            remote_id: channel.remote_id,
            title: decodeHTML(channel.title),
            thumbnail: channel.thumbnail,
            snippet: decodeHTML(channel.snippet),
            views: channel.views_count || '--',
            subscribers: channel.subscribers_count || '--',
            videos: channel.videos_count || '--',
            time_published: moment(channel.time_published).fromNow(),
            videos_url: urls.frontend.channel + channel.remote_id,
            external_url: urls.external.channel + channel.remote_id,
          };

          $("#channels .row").append(channels_search.templates.channel(channel_data));
        }

        if(resp.next_page){
          $("#channels").append(channels_search.templates.search_more({ entities: "channels" }));
          $("#channels .search-well").on("click", function(){
            $(this).remove();
            $("#channels").append(channels_search.templates.search_spinner());
            channels_search.search();
          });
        }
      }

      channels_search.page_token = (resp.next_page) ? resp.next_page : null;
      channels_search.loading = false;
    });
  },
  reset: function(){
    channels_search.query = "";
    channels_search.page_token = undefined;
    channels_search.loading = false;
    $("#inputSearchChannel").val("");
    $("#channels .row").html("");
    $("#channels .search-well").remove();
    $("#channels").append(channels_search.templates.search_home({entities: "channels"}));
  }
}
