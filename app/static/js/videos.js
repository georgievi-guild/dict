var videos_search = {
  entities: undefined,
  query: undefined,
  related_video: undefined,
  channel: undefined,
  playlist: undefined,
  order: undefined,
  page_token: undefined,
  loading: false,
  templates: undefined,
  init: function(query, related_video, channel, playlist, order){
    videos_search.query = query || "";
    videos_search.related_video = related_video || null;
    videos_search.channel = channel || null;
    videos_search.playlist = playlist || null;
    videos_search.order = order || "videoCount";

    if (videos_search.related_video){
      videos_search.entities = "related videos";
    } else if (videos_search.channel){
      videos_search.entities = "videos in the channel";
    } else if (videos_search.playlist){
      videos_search.entities = "videos in the playlist";
    } else {
      videos_search.entities = "videos";
    }

    $("#videos").append('<div class="row"></div>');

    videos_search.templates = {
      video: Handlebars.compile($("#video-snippet").html()),
      search_home: Handlebars.compile($("#search-home").html()),
      search_no_results: Handlebars.compile($("#search-no-results").html()),
      search_spinner: Handlebars.compile($("#search-spinner").html()),
      search_more: Handlebars.compile($("#search-more").html()),
    };
    videos_search.search();
  },
  search: function(){
    if (videos_search.loading){ return; }
    if (videos_search.page_token === null){ return; }
    videos_search.loading = true;

    $("#videos .search-well").remove();
    $("#videos").append(videos_search.templates.search_spinner());
    $("#inputSearchVideos").val(videos_search.query);

    var url, req;

    if(videos_search.query){
      url = urls.api.videos_search;
      req = {
        q: videos_search.query,
        order: videos_search.order,
        page_token: videos_search.page_token
      };
    } else if(videos_search.channel){
      url = urls.api.channel_videos;
      req = {
        channel: videos_search.channel,
        order: videos_search.order,
        page_token: videos_search.page_token
      };
    } else if(videos_search.playlist){
      url = urls.api.playlist_videos;
      req = {
        playlist: videos_search.playlist,
        order: videos_search.order,
        page_token: videos_search.page_token
      };
    } else if(videos_search.related_video){
      url = urls.api.video_related;
      req = {
        video: videos_search.related_video,
        order: videos_search.order,
        page_token: videos_search.page_token
      };
    } else {
        videos_search.reset();
        return;
    }

    $.getJSON(url, req, function(resp){
      if (!videos_search.loading){ return; }

      $("#videos .search-well").remove();

      if(resp.videos.length === 0){
        if (videos_search.page_token === null){

        } else {
          $("#videos").append(videos_search.templates.search_no_results({ entities: videos_search.entities }));
        }
      } else {
        for(var i in resp.videos){
          var video = resp.videos[i];

          if (video.channel_id === null){ continue; }

          var video_data = {
            remote_id: video.remote_id,
            title: decodeHTML(video.title),
            thumbnail: video.thumbnail,
            snippet: decodeHTML(video.snippet),
            time_published: moment(video.time_published).fromNow(),
            channel_id: video.channel_id,
            channel_title: video.channel_title,
            video_url: urls.frontend.video + video.remote_id,
            channel_url: urls.frontend.channel + video.channel_id,
            external_url: urls.external.video + video.remote_id,
            channel_external_url: urls.external.channel + video.channel_id,
          };

          $("#videos .row").append(videos_search.templates.video(video_data));
        }

        if(resp.next_page){
          $("#videos").append(videos_search.templates.search_more({ entities: videos_search.entities }));
          $("#videos .search-well").on("click", function(){
            $(this).remove();
            $("#videos").append(videos_search.templates.search_spinner());
            videos_search.search();
          });
        }
      }

      videos_search.page_token = (resp.next_page) ? resp.next_page : null;
      videos_search.loading = false;
    });
  },
  reset: function(){
    videos_search.query = "";
    videos_search.related_video = null;
    videos_search.channel = null;
    videos_search.playlist = null;
    videos_search.page_token = undefined;
    videos_search.loading = false;
    $("#inputSearchVideos").val("");
    $("#videos .row").html("");
    $("#videos .search-well").remove();
    $("#videos").append(videos_search.templates.search_home({entities: videos_search.entities }));
  }
}
