import os
import uuid


import click
from flask.cli import with_appcontext


from app.conf import Settings


@click.command(name="seed")
@with_appcontext
def do_seed():
    from app.models import DicitionaryItem, db
    path = os.path.join(Settings.BASEDIR, "data")
    for file in os.listdir(path):
        group = file[:-3]
        file_path = os.path.join(path, file)

        with open(file_path, "r", encoding="utf8") as fp:
            for line in fp.readlines():
                ngram = line.strip()
                if not ngram:
                    continue

                item = DicitionaryItem.query.filter_by(ngram=ngram, group=group).first()
                if not item:
                    item = DicitionaryItem(uuid=str(uuid.uuid4()), group=group, ngram=ngram)
                    db.session.add(item)
                    db.session.commit()
