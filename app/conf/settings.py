import os


class Settings(object):
    BASEDIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

    LOGS_PATH = os.path.join(BASEDIR, "logs")
    BASE_URL = os.environ.get('BASE_URL') or 'http://localhost:4040'

    RUN_PORT = 4040

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'dict.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_PRE_PING = True
    # SQLALCHEMY_ENGINE_OPTIONS = {'pool_size': None, 'pool_timeout': None}
