from flask import render_template, request
from app.models import DicitionaryItem


def ngrams():
    items = {}
    for item in DicitionaryItem.query.order_by("ngram").all():
        if item.group not in items:
            items[item.group] = []
        items[item.group].append(item)

    return render_template(
        'ngrams.html',
        items=items,
        group=request.args.get("group", None),
    )
